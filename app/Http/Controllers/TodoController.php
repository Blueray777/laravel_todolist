<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Todo;

class TodoController extends Controller
{
    public function index(){
      return Todo::where('completed', 0)->orderBy('id', 'desc')->get();
    }
    public function completed(){
        return Todo::where('completed', 1)->orderBy('id', 'desc')->get();
    }
    public function allTasks(){
        // return Todo::where('archive', 0)
        //return Todo::all()->orderBy('id', 'desc')->get();
        //return Todo::orderBy('id', 'desc')->orderBy('completed', 'asc')->get();
        return Todo::orderBy('completed', 'asc')->orderBy('id', 'desc')->get();
    }
    public function store(Request $request){
      $this->validate($request, [
            'items' => 'required|max:50'
        ]);
        return Todo::create(['items' => request('items')]);
    }
    public function edit(Request $request){
        $this->validate($request, [
            'items' => 'required|max:50'
        ]);
        $task = Todo::findOrFail($request->id);
        $task->items = $request->items;
        $task->save();
    }
    public function done($id){
        $task = Todo::findOrFail($id);
        $task->completed = ! $task->completed;
        $task->save();
    }
    public function destroy($id){
        $task = Todo::findOrFail($id);
        $task->delete();
    }
}

