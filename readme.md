# Laradock / Laravel / vue でToDoList

ToDo ListをLaradockとLaravelで構築する  
フロントにVueを使用した  
ディレクトリ構造などは以下に記載  
デプロイ先はheroku

- [デモサイト](https://alpha-todo-list.herokuapp.com/)
- [ToDoList アプリについて](https://gitlab.com/Blueray777/laravel_todolist/-/blob/master/aboutTodoList.md)


## Laradock & Laravel projectのはじめ方

### インストール手順参考URL
- [Laradock](http://laradock.io/)
- [Laradockで簡単に開発環境を準備するメモ](https://nogson2.hatenablog.com/entry/2018/11/04/174328)
- [Laradockで簡単に開発環境を準備するメモ2](https://nogson2.hatenablog.com/entry/2018/11/12/152215)
- [Laradockを使ったLaravel開発環境構築のやさしい解説](https://techblog.scouter.co.jp/entry/2019/01/11/133508)
- [Laradockが上手く動かなくて困った話](https://qiita.com/skmt719/items/a296d81150fd7319e71a)

### 1.LaradockのリポジトリをCloneする
Laradockを自分のプロジェクト内のディレクトリにcloneする

**Laradock**
[Laradock](http://laradock.io/)

**Laradock github**
[https://github.com/laradock/laradock](https://github.com/laradock/laradock)

**git clone**
`git clone https://github.com/laradock/laradock.git　my-projecut`

ディレクトリ構成は以下のようになる

```
∟ my-project
```


### 2.「.env」ファイル作成
Laradockディレクトリ内に移動し、env-exampleをコピーしリネームする

```
cd my-project

cp env-example .env
```

MYSQL_VERSIONが「lasted」なので、最新バージョンのMySQL Docker imageがpullされる   
そのためphpとの相互性の良い「5.7」に変更する  
念のため、コメントアウトして変更する

```
# MYSQL_VERSION=latest
MYSQL_VERSION=5.7
```

### 3.コンテナ起動
`docker-compose up`してNginxとMySQLのコンテナを起動する

```
docker-compose up -d nginx mysql
```

初回はかなり時間がかかる
`docker-compose up`中にでたエラーは以下参照
- [docker-compose up のエラー](#docker-compose-up-のエラー)


### 4.Laravelをインストール
立ち上がったら、Laradockにログインする

```
docker exec -it laradock_workspace_1 /bin/bash
```

Composerをインストール

```
root@7651bdd4c89c:/var/www#
composer create-project laravel/laravel src
```

このリポジトリのLaravelのバージョンは「Laravel Framework 5.8.26」

**Laravel**
[Laravel](https://laravel.com/)

**Composer**
[https://getcomposer.org/](https://getcomposer.org/)

### 5.コンテナ停止
Laradockの「.env」ファイルを編集するので一旦コンテナを停止する

```
docker-compose stop
```

### 6.「.env」の編集
LaradockからLaravelにアクセスできるようにパスを通す
Laradockの「.env」ファイルの`APP_CODE_PATH_HOST=../`にLaravelのディレクトリ名を記載する

念の為`APP_CODE_PATH_HOST=../`をコメントアウトする

```
# APP_CODE_PATH_HOST=../
APP_CODE_PATH_HOST=../src
```

### 7.MySQLの環境設定を編集
Laradockの「.env」とLaravelの「.env」のMySQLに関する環境設定の内容を揃える

#### Laradock .env

```
MYSQL_VERSION=latest
MYSQL_VERSION=5.7
MYSQL_DATABASE=default
MYSQL_USER=default
MYSQL_PASSWORD=secret
MYSQL_PORT=3306
MYSQL_ROOT_PASSWORD=root
MYSQL_ENTRYPOINT_INITDB=./mysql/docker-entrypoint-initdb.d
```

#### Laravel .env
1. `DB_HOST=127.0.0.1`を`DB_HOST=mysql`に変更
2. `DB_DATABASE=homestead`をLaradockの`MYSQL_DATABASE=default`に合わせて`DB_DATABASE=default`に変更
3. `DB_USERNAME=homestead`をLaradockの`MYSQL_USER=default`に合わせて`DB_USERNAME=default`に変更
4. `MYSQL_PASSWORD=secret`と`DB_PASSWORD=secret`の値を同じにする

```
DB_CONNECTION=mysql
# DB_HOST=127.0.0.1
DB_HOST=mysql
DB_PORT=3306
# DB_DATABASE=homestead
DB_DATABASE=default
# DB_USERNAME=homestead
DB_USERNAME=default
DB_PASSWORD=secret
```

### 7.コンテナ再起動
```
docker-compose up -d nginx mysql
```

### 8.MySQLにログインする
以下のコマンドでMySQLコンテナにログインする
```
docker exec -it laradock_mysql_1 /bin/bash
```

MySQLコマンド`mysql -u default -p`でログインする

```
root@aa476699c87a:/# mysql -u default -p
```

`Enter password:`と聞かれたら`DB_PASSWORD=secret`で記載した「secret」をいれる

```
Enter password:
```

以下が表示されたらMySQLにログイン成功

```
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 5
Server version: 5.7.26 MySQL Community Server (GPL)

Copyright (c) 2000, 2019, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql>
```

### Docker コマンド
コンテナ起動
- docker-compose up -d nginx mysql

MySQLにログイン
- docker exec -it laradock_mysql_1 /bin/bash

Laradockにログイン
- docker exec -it laradock_workspace_1 /bin/bash

コンテナを止める
- docker-compose stop

### MySQLの日本語設定の仕方
以下を参照した  
- [Docker-Composeで立てたMySQL用コンテナの日本語設定の仕方](https://qiita.com/maejima_f/items/4d5432aa471a9deaea7f)

#### MySQLコンテナにvimをインストールする方法
以下を参照した  
- [Docker — docker コンテナの中で vim が使えない場合](https://qiita.com/YumaInaura/items/3432cc3f8a8553e05a6e)

Ubuntu系なので、コンテナに入り以下を叩いた

```
apt-get update
apt-get install vim
```

### MySQL コマンド

MySQLにログイン
- mysql -u default -p

データベースを見る
- show databases;

使用するデータベースを選択する
- use nameOfDatabase;

テーブルの構造を確認する
- desc nameOfTable;

テーブル一覧を確認
- show tables;

テーブルのデータを確認
- select * from nameOfTable;

テーブル内のカラムに関する情報を表示
- show columns from nameOfTable;

テーブルに格納されているデータを削除
- delete from nameOfTable;

テーブルを削除
- drop tables nameOfTable;

### artisan コマンド

コマンド一覧を確認する
- php artisan list

Laravelのversionを確認する
- php artisan --version

.envのkeyをsetする
- php artisan key:generate

マイグレーション実行
- php artisan migrate

一旦全てのテーブルを削除してマイグレーションし直す
- php artisan migrate:fresh

データベースへ初期値を設定する
- php artisan db:seed

### コンパイルとサーバー起動
- npm run dev
- php artisan serve


### docker-compose up のエラー

#### node でエラー
「Error: Node version 'stable' not found when i up workspace」と怒られる
.evnの「WORKSPACE_NODE_VERSION」を「v8.12.0」に変えて回避

**参考URL**
- [Error: Node version 'stable' not found when i up workspace](https://github.com/laradock/laradock/issues/1564#issuecomment-423705186)

#### Yarn でエラー
wifiなどの通信速度が遅いとか他の通信エラーなど、通信状況によってエラーが出ることがある
再挑戦してみると意外といける

**参考URL**
- [Bad signature on install](https://github.com/yarnpkg/yarn/issues/6156)

#### Nginx のビルドでエラー
依存しているDocker HubのNginxコンテナもしくはalpineコンテナがアップデートされたことが原因
Nginxの dockerfile を最新のマスターマージされた物に変更
https://github.com/laradock/laradock/blob/master/Nginx/Dockerfile
- [Laradockコンテナのビルドで ”adduser: group 'www-data' in use” が発生したときの対応【メモ】](https://imanengineer.net/laradock-build-faild-adduser-group-www-data-in-use/)

#### そんなディレクトリ知らないよエラー

```
Cannot start service docker-in-docker: b'Mounts denied: \r\nThe path /usr/local/development/laravel/laravelTodo/laravelTodo\r\nis not shared from OS X and is not known to Docker.
```

Docker for Macの「設定」-「共有ディレクトリ」でパスを登録してリスタート

- [【Docker For Mac】Error response from daemon: Mounts deniedの対応](https://qiita.com/nishina555/items/a75ce530d9382aa09511)

### Nginxが立ち上がっているのにHTTPで500エラー
Laradockのgitイシューのコメントに「Dockerのlogを見てみたら？」ってのがあったので、logを確認した

- Dockerのlogを見るコマンド
`docker logs -f $(docker ps | grep php-fpm | awk -F $'[ ]{2,}' '{ print $7 }')`  

すると、こんなこと言ってた「【../vendor/autoload.php】が見つからないYo!」

```
[29-Jun-2019 08:30:37] NOTICE: fpm is running, pid 1
[29-Jun-2019 08:30:37] NOTICE: ready to handle connections
[29-Jun-2019 08:31:02] WARNING: [pool www] child 7 said into stderr: "NOTICE: PHP message: PHP Warning:  require(/var/www/public/../vendor/autoload.php): failed to open stream: No such file or directory in /var/www/public/index.php on line 24"
[29-Jun-2019 08:31:02] WARNING: [pool www] child 7 said into stderr: "NOTICE: PHP message: PHP Fatal error:  require(): Failed opening required '/var/www/public/../vendor/autoload.php' (include_path='.:/usr/local/lib/php') in /var/www/public/index.php on line 24"
172.23.0.6 -  29/Jun/2019:08:31:02 +0000 "GET /index.php" 500
```

調べると、Composerがインストールされていないのが原因っぽい
gitをpullしただけじゃComposerはインストールされないものね（テヘペロっ！失態だぜ！）
Laravelのディレクトリに移動してComposerをインストール
`brew install homebrew/core/composer`

その後Composerをupdeteする
`composer update`

Laravelの「.env」の設定が「.env.example」をコピーしただけで初期値のままになっていると、http://localhost:8000にアクセスするした時に「No application encryption key has been specified.」とLaravelのエラーが表示される
そんな時は、Laradockのworkspaceに入ってartisanコマンドでkeyのセットをする

```
% docker exec -it laradock_workspace_1 /bin/bash
root@5d21e655db15:/var/www# php artisan key:generate
Application key set successfully.
```
Laravelの.envを確認してみると値がセットされている

その後、http://localhost:8000にアクセスするとLaravelのページが表示される

**参考URL**
- [Keep getting Nginx error 500](https://github.com/laradock/laradock/issues/172)
- [Laravelで…vendor/autoload.php : failed to open stream: No such file or directory というエラーによりデフォルト画面が表示されない解決方法](https://qiita.com/pugiemonn/items/3d000ac0486987dd92df)
- [Composerインストールでhomebrew/php deprecatedになるとき](https://qiita.com/wktq/items/ac37cc023b1f7db8bd86)
- [No application encryption key has been specified.となったときの対応方法](https://qiita.com/ponsuke0531/items/197c76fcb9300d7c5f36)
