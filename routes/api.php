<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::group(['middleware' => 'api'], function() {
    Route::get('current_tasks', 'TodoController@index');
    Route::get('completed_tasks', 'TodoController@completed');
    Route::get('all_tasks', 'TodoController@allTasks');
    Route::post('create_task', 'TodoController@store');
    Route::post('delete_task/{id}', 'TodoController@destroy');
    Route::post('done_task/{id}', 'TodoController@done');
    Route::post('edit_task', 'TodoController@edit');
});
