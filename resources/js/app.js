/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// require('./bootstrap');
// load all dependencies
import axios from 'axios';
//import Datepicker from 'vuejs-datepicker';
//import Modal from "./components/modal";
window.Vue = require('vue');
window.axios = axios;
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

// load all components
Vue.component('task-list', require('./components/Task-list.vue').default);
//Vue.component('modal-area', require('./components/modal.vue').default);
// Vue.component('task-list', require('./components/main.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
  el: '#app'
});

// const modal = new Vue({
//   el: '#modalArea',
//   components: {
//     Modal
//   },
//   data() {
//     return {
//       showModal: false
//     }
//   },
//   methods: {
//     openModal() {
//       this.showModal = true
//     },
//     closeModal() {
//       this.showModal = false
//     }
//   },
// })


// const date_picker = new Vue({
//   el: '#date_picker',
//   components: {
//     Datepicker
//   },
//   data: {
//     date: new Date(),
//     DatePickerFormat: 'yyyy-MM-dd'
//   }
// });
