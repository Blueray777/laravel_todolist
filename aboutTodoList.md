# ToDoList アプリについて

## Vueの構造

```
.resources
|- js
   |- components
   |   |-modal.vue // For Modal
   |   |-Task-list.vue // For Task List 
   |-app.js
```

## migrateについて

### エラー解決方法
`php artisan migrate` でエラーがでた

```
root@24787dbb37d0:/var/www# php artisan migrate

   Illuminate\Database\QueryException  : SQLSTATE[HY000] [1045] Access denied for user 'root'@'172.23.0.4' (using password: YES) (SQL: select * from information_schema.tables where table_schema = default and table_name = migrations and table_type = 'BASE TABLE')

  at /var/www/vendor/laravel/framework/src/Illuminate/Database/Connection.php:664
    660|         // If an exception occurs when attempting to run a query, we'll format the error
    661|         // message to include the bindings with SQL, which will make this exception a
    662|         // lot more helpful to the developer instead of just the database's errors.
    663|         catch (Exception $e) {
  > 664|             throw new QueryException(
    665|                 $query, $this->prepareBindings($bindings), $e
    666|             );
    667|         }
    668|

  Exception trace:

  1   PDOException::("SQLSTATE[HY000] [1045] Access denied for user 'root'@'172.23.0.4' (using password: YES)")
      /var/www/vendor/laravel/framework/src/Illuminate/Database/Connectors/Connector.php:70

  2   PDO::__construct("mysql:host=mysql;port=3306;dbname=default", "root", "secret", [])
      /var/www/vendor/laravel/framework/src/Illuminate/Database/Connectors/Connector.php:70

  Please use the argument -v to see more details.
```

Laradockの.envとLaravelの.envの「DATABESE」と「USERNAME」は同じ、何度かdocker-compose upをやり直しても同じエラーが出るので、試しにLaravelの.envを「.-env」にリネームしてどうなるかやってみたとことろ、migreteできてしまった

```
root@24787dbb37d0:/var/www# php artisan migrate
Migration table created successfully.
Migrating: 2014_10_12_000000_create_users_table
Migrated:  2014_10_12_000000_create_users_table
Migrating: 2014_10_12_100000_create_password_resets_table
Migrated:  2014_10_12_100000_create_password_resets_table
Migrating: 2019_05_13_142813_create_todos_table
Migrated:  2019_05_13_142813_create_todos_table
```
### MySQLについて
MySQLにログインして、確認してみると「default」databaseの中に「todos」のtableができていた

```
mysql> show tables;
+-------------------+
| Tables_in_default |
+-------------------+
| migrations        |
| password_resets   |
| todos             |
| users             |
+-------------------+
```

## コンポーネントを記載する場所
コンポーネントの呼出は必ずDOMの中にする
e.g
```
<div>
<Modal></Modal>
</div>
```

## 削除確認モーダル

### コンポーネントの親子関係

```
TodoListコンポーネント
 |- モーダルコンポーネント
```

TodoListコンポーネントでタスクアイテムを削除する際に、削除ボタン押下後に削除確認をするモーダルを出す    
TodoListコンポーネント内のModalコンポーネント呼び出しに、確認文言とボタンパーツを設置  
モーダルコンポーネント内にslotで渡す  

この方法で、モーダルに使用するメソッドをTodoListに記載しモーダル間通信はslotのみ  
v-forで入れる連想配列のIDを「モーダルを開く」ボタンで、変数にセット  
削除メソッドの引数に対象の変数を入れ、データベースにID番号を送り削除させる  

