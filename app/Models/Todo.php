<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Todo extends Model{
    // モデルとテーブルを紐付け
    protected $table = 'todos';
    // プライマリーキーを'id'に紐付け
    // protected $praimaryKey = 'id';
    // タイムスタンプを無効にする
    // public $timestamps = false;
    protected $guarded = [];
}
