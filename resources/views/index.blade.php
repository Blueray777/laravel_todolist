<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>
    <!-- Fonts -->
    <link rel="stylesheet" href="https://use.typekit.net/buh5rof.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <!-- Script -->
    <script>
        window.Laravel = { csrfToken: '{{ csrf_token() }}' }
    </script>
</head>

<body>
    <div id="wrapper">
        <header id="header">
            <h1 class="header--hedding">Todo List</h1>
        </header>
        <!-- <div id="date_picker">
            <datepicker input-class="input" v-model="date" :format="DatePickerFormat"></datepicker>
        </div> -->
        <!-- / #date_picker -->
        <div id='app'>
            <task-list></task-list>
        </div>
        <!-- / #app -->
    </div>
    <!-- / #wrapper -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
